PROJECT MANAGEMENT SYSTEM
-------------------------
This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Setup and integration tests
---------------------------
```
npm install -g create-react-app
npm install
npm test
```
```
 PASS  src/integrationTests/integration.test.js
  ✓ 0 - Create new employees with a given first name, last name and a direct supervisor (2ms)
  ✓ 1 - Create new projects with a given name, a start date and a time slack (1ms)
  ✓ 2 - Creating a new task with a given name, a description of this task and estimated days needed to complete it (1ms)
  ✓ 3 - Assign task to a project - project's end date should be updated (2ms)
  ✓ 4 - Update task druation, project's tasks and end dates should be updated (1ms)
  ✓ 5 - Assign a project to an employee (an employee can only work on two projects at the same time (1ms)
  ✓ 6 - Delete a task, underlying references should be updated (1ms)
  ✓ 7 - Delete a project, underlying references should be updated (1ms)
  ✓ 8 - Get all employees
  ✓ 9 - Get all tasks for a project - empty tasks (1ms)
  ✓ 10 - Get all tasks for a project - non-empty tasks
  ✓ 11 - Getting the total days needed for a given list of projects - single project
  ✓ 12 - Getting the total days needed for a given list of projects - multiple projects (1ms)
  ✓ 13 - Test whole state
```


Good place to start exploring the code
--------------------------------------
`src/integrationTests/integration.test.js`

Technologies used
--------------------------------------
+ [Redux](https://github.com/reactjs/redux): To manage the whole state of the application.
+ [Reselect](https://github.com/reactjs/reselect): To compute memoized derived data such as project's end date. Selectors are also used throught the application as the standard pattern for getters.