import { createSelector } from "reselect";

/**
 * Sum days to date object
 */
const addDaysToDate = function(date, days) {
  const newDate = new Date(date.year, date.month, date.date);
  newDate.setDate(newDate.getDate() + days);
  return {
    year: newDate.getFullYear(),
    month: newDate.getMonth(),
    date: newDate.getDate()
  };
};

/**
 * @return {number} Sum of durations of all tasks
 */
const getTasksTime = function(tasks) {
  let time = 0;
  tasks.forEach(task => {
    time += task.timeDuration;
  });
  return time;
};

/**
 * Get single project's duration
 */
const getProjectDuration = function(project, tasks) {
  const projectTasks = project.tasks.map(taskId => {
    return tasks[taskId];
  });
  return getTasksTime(projectTasks) + project.slackTime;
};

export const getProjectEndDate = createSelector(
  (state, projectId) => state.projects[projectId],
  (state, projectId) => state.tasks,

  (project, tasks) => {
    const projectDuration = getProjectDuration(project, tasks);
    return addDaysToDate(project.startDate, projectDuration);
  }
);

export const getAllEmployees = createSelector(
  state => state.employees,
  employees => employees
);

export const getTasksByProject = createSelector(
  (state, projectId) => state.projects[projectId],
  (state, projectId) => state.tasks,
  (project, tasks) => {
    return project.tasks.map(taskId => {
      return tasks[taskId];
    });
  }
);

/**
 * @param {object} state
 * @param {array} project ids
 */
export const getProjectsDuration = createSelector(
  (state, projectIds) => {
    return projectIds.map(id => {
      return state.projects[id];
    });
  },
  state => state.tasks,
  (projects, tasks) => {
    let duration = 0;

    Object.keys(projects).forEach(id => {
      let project = projects[id];
      duration += getProjectDuration(project, tasks);
    });
    return duration;
  }
);
