export default {
  0: {
    employees: {
      "0": {
        projects: [],
        id: 0,
        firstName: "Charles",
        lastName: "Mingus",
        supervisor: false
      },
      "1": {
        projects: [],
        id: 1,
        firstName: "Ray",
        lastName: "Brown",
        supervisor: 0
      },
      "2": {
        projects: [],
        id: 2,
        firstName: "Ron",
        lastName: "Carter",
        supervisor: 1
      }
    },
    projects: {},
    tasks: {}
  },
  1: {
    employees: {
      "0": {
        projects: [],
        id: 0,
        firstName: "Charles",
        lastName: "Mingus",
        supervisor: false
      },
      "1": {
        projects: [],
        id: 1,
        firstName: "Ray",
        lastName: "Brown",
        supervisor: 0
      },
      "2": {
        projects: [],
        id: 2,
        firstName: "Ron",
        lastName: "Carter",
        supervisor: 1
      }
    },
    projects: {
      "0": {
        tasks: [],
        id: 0,
        name: "project 0",
        startDate: {
          year: 2018,
          month: 0,
          date: 1
        },
        slackTime: 5
      },
      "1": {
        tasks: [],
        id: 1,
        name: "project 1",
        startDate: {
          year: 2018,
          month: 1,
          date: 1
        },
        slackTime: 10
      }
    },
    tasks: {}
  },
  2: {
    employees: {
      "0": {
        projects: [],
        id: 0,
        firstName: "Charles",
        lastName: "Mingus",
        supervisor: false
      },
      "1": {
        projects: [],
        id: 1,
        firstName: "Ray",
        lastName: "Brown",
        supervisor: 0
      },
      "2": {
        projects: [],
        id: 2,
        firstName: "Ron",
        lastName: "Carter",
        supervisor: 1
      }
    },
    projects: {
      "0": {
        tasks: [],
        id: 0,
        name: "project 0",
        startDate: {
          year: 2018,
          month: 0,
          date: 1
        },
        slackTime: 5
      },
      "1": {
        tasks: [],
        id: 1,
        name: "project 1",
        startDate: {
          year: 2018,
          month: 1,
          date: 1
        },
        slackTime: 10
      }
    },
    tasks: {
      "0": {
        id: 0,
        name: "task 0",
        description: "description of task 0",
        timeDuration: 3
      }
    }
  },
  3: {
    employees: {
      "0": {
        projects: [],
        id: 0,
        firstName: "Charles",
        lastName: "Mingus",
        supervisor: false
      },
      "1": {
        projects: [],
        id: 1,
        firstName: "Ray",
        lastName: "Brown",
        supervisor: 0
      },
      "2": {
        projects: [],
        id: 2,
        firstName: "Ron",
        lastName: "Carter",
        supervisor: 1
      }
    },
    projects: {
      "0": {
        tasks: [0],
        id: 0,
        name: "project 0",
        startDate: {
          year: 2018,
          month: 0,
          date: 1
        },
        slackTime: 5
      },
      "1": {
        tasks: [],
        id: 1,
        name: "project 1",
        startDate: {
          year: 2018,
          month: 1,
          date: 1
        },
        slackTime: 10
      }
    },
    tasks: {
      "0": {
        id: 0,
        name: "task 0",
        description: "description of task 0",
        timeDuration: 3
      }
    }
  },
  4: {
    employees: {
      "0": {
        projects: [],
        id: 0,
        firstName: "Charles",
        lastName: "Mingus",
        supervisor: false
      },
      "1": {
        projects: [],
        id: 1,
        firstName: "Ray",
        lastName: "Brown",
        supervisor: 0
      },
      "2": {
        projects: [],
        id: 2,
        firstName: "Ron",
        lastName: "Carter",
        supervisor: 1
      }
    },
    projects: {
      "0": {
        tasks: [0],
        id: 0,
        name: "project 0",
        startDate: {
          year: 2018,
          month: 0,
          date: 1
        },
        slackTime: 5
      },
      "1": {
        tasks: [],
        id: 1,
        name: "project 1",
        startDate: {
          year: 2018,
          month: 1,
          date: 1
        },
        slackTime: 10
      }
    },
    tasks: {
      "0": {
        id: 0,
        name: "task 0",
        description: "description of task 0",
        timeDuration: 10
      }
    }
  },
  5: {
    employees: {
      "0": {
        projects: [0, 1],
        id: 0,
        firstName: "Charles",
        lastName: "Mingus",
        supervisor: false
      },
      "1": {
        projects: [],
        id: 1,
        firstName: "Ray",
        lastName: "Brown",
        supervisor: 0
      },
      "2": {
        projects: [],
        id: 2,
        firstName: "Ron",
        lastName: "Carter",
        supervisor: 1
      }
    },
    projects: {
      "0": {
        tasks: [0],
        id: 0,
        name: "project 0",
        startDate: {
          year: 2018,
          month: 0,
          date: 1
        },
        slackTime: 5
      },
      "1": {
        tasks: [],
        id: 1,
        name: "project 1",
        startDate: {
          year: 2018,
          month: 1,
          date: 1
        },
        slackTime: 10
      }
    },
    tasks: {
      "0": {
        id: 0,
        name: "task 0",
        description: "description of task 0",
        timeDuration: 10
      }
    }
  },
  6: {
    employees: {
      "0": {
        projects: [0, 1],
        id: 0,
        firstName: "Charles",
        lastName: "Mingus",
        supervisor: false
      },
      "1": {
        projects: [],
        id: 1,
        firstName: "Ray",
        lastName: "Brown",
        supervisor: 0
      },
      "2": {
        projects: [],
        id: 2,
        firstName: "Ron",
        lastName: "Carter",
        supervisor: 1
      }
    },
    projects: {
      "0": {
        tasks: [],
        id: 0,
        name: "project 0",
        startDate: {
          year: 2018,
          month: 0,
          date: 1
        },
        slackTime: 5
      },
      "1": {
        tasks: [],
        id: 1,
        name: "project 1",
        startDate: {
          year: 2018,
          month: 1,
          date: 1
        },
        slackTime: 10
      }
    },
    tasks: {}
  },
  7: {
    employees: {
      "0": {
        projects: [1],
        id: 0,
        firstName: "Charles",
        lastName: "Mingus",
        supervisor: false
      },
      "1": {
        projects: [],
        id: 1,
        firstName: "Ray",
        lastName: "Brown",
        supervisor: 0
      },
      "2": {
        projects: [],
        id: 2,
        firstName: "Ron",
        lastName: "Carter",
        supervisor: 1
      }
    },
    projects: {
      "1": {
        tasks: [],
        id: 1,
        name: "project 1",
        startDate: {
          year: 2018,
          month: 1,
          date: 1
        },
        slackTime: 10
      }
    },
    tasks: {}
  },
  8: {
    "0": {
      projects: [1],
      id: 0,
      firstName: "Charles",
      lastName: "Mingus",
      supervisor: false
    },
    "1": {
      projects: [],
      id: 1,
      firstName: "Ray",
      lastName: "Brown",
      supervisor: 0
    },
    "2": {
      projects: [],
      id: 2,
      firstName: "Ron",
      lastName: "Carter",
      supervisor: 1
    }
  },
  9: [],
  10: [
    {
      id: 1,
      name: "task 1",
      description: "description of task 1",
      timeDuration: 5
    },
    {
      id: 2,
      name: "task 2",
      description: "description of task 2",
      timeDuration: 7
    }
  ],
  13: {
    employees: {
      "0": {
        projects: [1],
        id: 0,
        firstName: "Charles",
        lastName: "Mingus",
        supervisor: false
      },
      "1": {
        projects: [],
        id: 1,
        firstName: "Ray",
        lastName: "Brown",
        supervisor: 0
      },
      "2": {
        projects: [],
        id: 2,
        firstName: "Ron",
        lastName: "Carter",
        supervisor: 1
      }
    },
    projects: {
      "1": {
        tasks: [1, 2],
        id: 1,
        name: "project 1",
        startDate: {
          year: 2018,
          month: 1,
          date: 1
        },
        slackTime: 10
      },
      "2": {
        tasks: [],
        id: 2,
        name: "project 2",
        startDate: {
          year: 2018,
          month: 2,
          date: 1
        },
        slackTime: 20
      }
    },
    tasks: {
      "1": {
        id: 1,
        name: "task 1",
        description: "description of task 1",
        timeDuration: 5
      },
      "2": {
        id: 2,
        name: "task 2",
        description: "description of task 2",
        timeDuration: 7
      }
    }
  }
};
