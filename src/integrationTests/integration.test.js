import { createStore } from "redux";
import expectStoreStates from "./expectStoreStates";

// REDUCERS
import reducers from "../reducers/index";

// ACTIONS
import {
  postEmployee,
  addProjectToEmployee
} from "../actions/employeesActions";
import {
  postProject,
  addTaskToProject,
  deleteProject
} from "../actions/projectsActions";
import {
  postTask,
  updateTaskDuration,
  deleteTask
} from "../actions/tasksActions";

// SELECTORS
import {
  getProjectEndDate,
  getAllEmployees,
  getTasksByProject,
  getProjectsDuration
} from "../selectors";

const store = createStore(reducers);
const expectStoreToEqual = function(obj) {
  expect(store.getState()).toEqual(obj);
};

test("0 - Create new employees with a given first name, last name and a direct supervisor", () => {
  store.dispatch(
    postEmployee({
      id: 0,
      firstName: "Charles",
      lastName: "Mingus",
      supervisor: false
    })
  );

  store.dispatch(
    postEmployee({
      id: 1,
      firstName: "Ray",
      lastName: "Brown",
      supervisor: 0
    })
  );

  store.dispatch(
    postEmployee({
      id: 2,
      firstName: "Ron",
      lastName: "Carter",
      supervisor: 1
    })
  );

  expectStoreToEqual(expectStoreStates[0]);
});

test("1 - Create new projects with a given name, a start date and a time slack", () => {
  store.dispatch(
    postProject({
      id: 0,
      name: "project 0",
      startDate: { year: 2018, month: 0, date: 1 },
      slackTime: 5
    })
  );

  store.dispatch(
    postProject({
      id: 1,
      name: "project 1",
      startDate: { year: 2018, month: 1, date: 1 },
      slackTime: 10
    })
  );

  expectStoreToEqual(expectStoreStates[1]);
});

test("2 - Creating a new task with a given name, a description of this task and estimated days needed to complete it", () => {
  store.dispatch(
    postTask({
      id: 0,
      name: "task 0",
      description: "description of task 0",
      timeDuration: 3
    })
  );

  expectStoreToEqual(expectStoreStates[2]);
});

test("3 - Assign task to a project - project's end date should be updated", () => {
  // get project end date - before adding task
  expect(getProjectEndDate(store.getState(), 0)).toEqual({
    date: 6,
    month: 0,
    year: 2018
  });
  store.dispatch(
    addTaskToProject({
      projectId: 0,
      taskId: 0
    })
  );
  // get project end date - after adding task
  expect(getProjectEndDate(store.getState(), 0)).toEqual({
    date: 9,
    month: 0,
    year: 2018
  });

  expectStoreToEqual(expectStoreStates[3]);
});

test("4 - Update task druation, project's tasks and end dates should be updated", () => {
  store.dispatch(
    updateTaskDuration({
      id: 0,
      timeDuration: 10
    })
  );

  // after updating task duration
  expect(getProjectEndDate(store.getState(), 0)).toEqual({
    date: 16,
    month: 0,
    year: 2018
  });

  expectStoreToEqual(expectStoreStates[4]);
});

test("5 - Assign a project to an employee (an employee can only work on two projects at the same time", () => {
  store.dispatch(
    addProjectToEmployee({
      employeeId: 0,
      projectId: 0
    })
  );
  store.dispatch(
    addProjectToEmployee({
      employeeId: 0,
      projectId: 1
    })
  );
  // this action will be ignored because employee reached max number of projects
  store.dispatch(
    addProjectToEmployee({
      employeeId: 0,
      projectId: 2
    })
  );

  expectStoreToEqual(expectStoreStates[5]);
});

test("6 - Delete a task, underlying references should be updated", () => {
  store.dispatch(deleteTask({ taskId: 0 }));
  expectStoreToEqual(expectStoreStates[6]);
});

test("7 - Delete a project, underlying references should be updated", () => {
  store.dispatch(deleteProject({ projectId: 0 }));
  expectStoreToEqual(expectStoreStates[7]);
});

test("8 - Get all employees", () => {
  expect(getAllEmployees(store.getState())).toEqual(expectStoreStates[8]);
});

test("9 - Get all tasks for a project - empty tasks", () => {
  expect(getTasksByProject(store.getState(), 1)).toEqual(expectStoreStates[9]);
});

test("10 - Get all tasks for a project - non-empty tasks", () => {
  // (1) add some tasks
  store.dispatch(
    postTask({
      id: 1,
      name: "task 1",
      description: "description of task 1",
      timeDuration: 5
    })
  );
  store.dispatch(
    postTask({
      id: 2,
      name: "task 2",
      description: "description of task 2",
      timeDuration: 7
    })
  );

  // (2) assigned tasks to project
  store.dispatch(
    addTaskToProject({
      projectId: 1,
      taskId: 1
    })
  );
  store.dispatch(
    addTaskToProject({
      projectId: 1,
      taskId: 2
    })
  );

  // (3) get tasks for project and test
  expect(getTasksByProject(store.getState(), 1)).toEqual(expectStoreStates[10]);
});

test("11 - Getting the total days needed for a given list of projects - single project", () => {
  expect(getProjectsDuration(store.getState(), [1])).toEqual(22);
});

test("12 - Getting the total days needed for a given list of projects - multiple projects", () => {
  store.dispatch(
    postProject({
      id: 2,
      name: "project 2",
      startDate: { year: 2018, month: 2, date: 1 },
      slackTime: 20
    })
  );
  expect(getProjectsDuration(store.getState(), [1, 2])).toEqual(42);
});

test("13 - Test whole state", () => {
  expectStoreToEqual(expectStoreStates[13]);
});
