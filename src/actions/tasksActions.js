export function postTask(task) {
  return {
    type: "POST_TASK",
    payload: task
  };
}

export function updateTaskDuration(payload) {
  return {
    type: "UPDATE_TASK_DURATION",
    payload: payload
  };
}

export function deleteTask(payload) {
  return {
    type: "DELETE_TASK",
    payload: payload
  };
}
