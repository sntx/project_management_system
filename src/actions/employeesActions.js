export function postEmployee(employee) {
  return {
    type: "POST_EMPLOYEE",
    payload: employee
  };
}

export function addProjectToEmployee(payload) {
  return {
    type: "ADD_PROJECT_TO_EMPLOYEE",
    payload: payload
  };
}
