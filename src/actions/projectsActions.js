export function postProject(project) {
  return {
    type: "POST_PROJECT",
    payload: project
  };
}

export function addTaskToProject(project) {
  return {
    type: "ADD_TASK_TO_PROJECT",
    payload: project
  };
}

export function deleteProject(project) {
  return {
    type: "DELETE_PROJECT",
    payload: project
  };
}
