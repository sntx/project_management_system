import update from "immutability-helper";

const doDeleteTask = function(state, taskId) {
  // (1) delete task from tasks
  const newState = update(state, {
    tasks: {
      $unset: [taskId]
    }
  });

  // (2) delete task from projects
  Object.keys(newState.projects).forEach(id => {
    let project = newState.projects[id];
    let tasks = project.tasks;
    let filteredTasks = tasks.filter(id => id !== taskId);

    project.tasks = filteredTasks;
  });

  return newState;
};

const doDeleteProject = function(state, projectId) {
  // (1) delete project from projects
  const newState = update(state, {
    projects: {
      $unset: [projectId]
    }
  });

  // (1) delete project from employees
  Object.keys(newState.employees).forEach(id => {
    let employee = newState.employees[id];
    let projects = employee.projects;
    let filteredProjects = projects.filter(id => id !== projectId);

    employee.projects = filteredProjects;
  });

  return newState;
};

export function rootReducers(state = {}, action) {
  switch (action.type) {
    case "DELETE_TASK":
      return doDeleteTask(state, action.payload.taskId);
    case "DELETE_PROJECT":
      return doDeleteProject(state, action.payload.projectId);
    default:
      break;
  }

  return state;
}
