import update from "immutability-helper";

const doPostProject = function(projects, project) {
  const defaults = { tasks: [] };
  const newProject = update(defaults, { $merge: project });

  return update(projects, {
    [project.id]: {
      $set: newProject
    }
  });
};

export function projectsReducers(projects = {}, action) {
  const payload = action.payload;

  switch (action.type) {
    case "POST_PROJECT":
      return doPostProject(projects, payload);

    case "ADD_TASK_TO_PROJECT":
      return update(projects, {
        [payload.projectId]: {
          tasks: {
            $push: [payload.taskId]
          }
        }
      });
    default:
      break;
  }

  return projects;
}
