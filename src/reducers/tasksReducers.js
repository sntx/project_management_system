import update from "immutability-helper";

export function tasksReducers(tasks = {}, action) {
  switch (action.type) {
    case "POST_TASK":
      return update(tasks, {
        [action.payload.id]: {
          $set: action.payload
        }
      });

    case "UPDATE_TASK_DURATION":
      return update(tasks, {
        [action.payload.id]: {
          timeDuration: {
            $set: action.payload.timeDuration
          }
        }
      });
    default:
      break;
  }

  return tasks;
}
