import update from "immutability-helper";

const MAX_PROJECTS = 2;

const doPostEmployee = function(employees, employee) {
  const defaults = { projects: [] };
  const newEmloyee = update(defaults, { $merge: employee });

  return update(employees, {
    [employee.id]: {
      $set: newEmloyee
    }
  });
};

const addProjectToEmployee = function(employees, payload) {
  const employeeId = payload.employeeId;
  const employee = employees[employeeId];

  // Ignore: employee reached max number of projects
  if (employee.projects.length >= MAX_PROJECTS) {
    return employees;
  }

  // Accept:
  return update(employees, {
    [employeeId]: {
      projects: {
        $push: [payload.projectId]
      }
    }
  });
};

export function employeesReducers(employees = {}, action) {
  const payload = action.payload;

  switch (action.type) {
    case "POST_EMPLOYEE":
      return doPostEmployee(employees, payload);
    case "ADD_PROJECT_TO_EMPLOYEE":
      return addProjectToEmployee(employees, payload);
    default:
      break;
  }

  return employees;
}
