import { rootReducers } from "./rootReducers";
import { employeesReducers } from "./employeesReducers";
import { projectsReducers } from "./projectsReducers";
import { tasksReducers } from "./tasksReducers";

/**
 * Custom combine reducers implementation. Allows to combine key-specific and root
 * reducers.
 */
const myCombineReducers = function(keyedReducers = {}, ...reducers) {
  return function(state = {}, action) {
    let newState = Object.assign({}, state);

    // run key-specific reducers
    newState = Object.keys(keyedReducers).reduce((prev, key) => {
      let reducer = keyedReducers[key];
      prev[key] = reducer(prev[key], action);
      return prev;
    }, newState);

    // run root reducers
    newState = reducers.reduce((prev, reducer) => {
      return reducer(prev, action);
    }, newState);

    return newState;
  };
};

export default myCombineReducers(
  {
    employees: employeesReducers,
    projects: projectsReducers,
    tasks: tasksReducers
  },
  rootReducers
);
